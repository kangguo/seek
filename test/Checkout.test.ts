import Checkout from '../src/Checkout'
import Item from '../src/Item'
import PricingRule from '../src/PricingRule'
import { calcN4M, calcWithDiscount } from '../src/Utils';
const classic3For1 = new PricingRule('Classic Ad',
    (itemCount: number, originalPrice: number) => {
        return calcN4M(itemCount, originalPrice, 3, 1);
    })
const standoutDiscount = new PricingRule('Standout Ad',
    (itemCount: number) => {
        return calcWithDiscount(itemCount, 299.99)
    })
const standout5For4 = new PricingRule('Standout Ad',
    (itemCount: number, originalPrice: number) => {
        return calcN4M(itemCount, originalPrice, 5, 1);
    })
const premiumDiscount = new PricingRule('Premium Ad',
    (itemCount: number) => {
        return calcWithDiscount(itemCount, 389.99)
    })

const customersRules = new Map<string, PricingRule[]>();
customersRules.set('Default', []);
customersRules.set('SecondBite', [classic3For1]);
customersRules.set('Axil Coffee Roasters', [standoutDiscount]);
customersRules.set('MYER', [standout5For4, premiumDiscount]);

const item1 = new Item('Classic Ad',
    'Offers the most basic level of advertisement', 269.99)
const item2 = new Item('Standout Ad',
    'Allows advertisers to use a company logo and use a longer presentation text', 322.99)
const item3 = new Item('Premium Ad',
    'Same benefits as Standout Ad, but also puts the advertisement at the top of the results, allowing higher visibility',
    394.99)

const checkout = Checkout.new([])
it("should return correct value for default", () => {
    checkout.clear()
    const rules = customersRules.get('Default');
    checkout.changeRules(rules ? rules : []);
    checkout.scan(item1)
    checkout.scan(item2)
    checkout.scan(item3)
    expect(checkout.total()).toBe(987.97)
})

it("should return correct value for SecondBite", () => {
    checkout.clear()
    const rules = customersRules.get('SecondBite');
    checkout.changeRules(rules ? rules : []);
    checkout.scan(item1)
    checkout.scan(item1)
    checkout.scan(item1)
    checkout.scan(item3)
    expect(checkout.total()).toBe(934.97)
})

it("should return correct value for Axil Coffee Roasters", () => {
    checkout.clear()
    const rules = customersRules.get('Axil Coffee Roasters');
    checkout.changeRules(rules ? rules : []);
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item3)
    expect(checkout.total()).toBe(1294.96)
})

it("should return correct value for MYER", () => {
    checkout.clear()
    const rules = customersRules.get('MYER');
    checkout.changeRules(rules ? rules : []);
    checkout.scan(item1)
    checkout.scan(item1)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item2)
    checkout.scan(item3)
    expect(checkout.total()).toBe(4159.87)
})