import Checkout from './Checkout'
import Item from './Item'
import PricingRule from './pricingRule'
import { calcN4M, calcWithDiscount } from './Utils';

const classic3For1 = new PricingRule('Classic Ad',
  (itemCount: number, originalPrice: number) => {
    return calcN4M(itemCount, originalPrice, 3, 1);
  })
const standoutDiscount = new PricingRule('Standout Ad',
  (itemCount: number) => {
    return calcWithDiscount(itemCount, 299.99)
  })
const standout5For4 = new PricingRule('Standout Ad',
  (itemCount: number, originalPrice: number) => {
    return calcN4M(itemCount, originalPrice, 5, 1);
  })
const premiumDiscount = new PricingRule('Premium Ad',
  (itemCount: number) => {
    return calcWithDiscount(itemCount, 389.99)
  })

const rules = new Map<string, PricingRule[]>();
rules.set('Default', []);
rules.set('SecondBite', [classic3For1]);
rules.set('Axil Coffee Roasters', [standoutDiscount]);
rules.set('MYER', [standout5For4, premiumDiscount]);

const item1 = new Item('Classic Ad',
  'Offers the most basic level of advertisement', 269.99)
const item2 = new Item('Standout Ad',
  'Allows advertisers to use a company logo and use a longer presentation text', 322.99)
const item3 = new Item('Premium Ad',
  'Same benefits as Standout Ad, but also puts the advertisement at the top of the results, allowing higher visibility',
  394.99)

const defaultRules = rules.get('Default')
const checkout = Checkout.new(defaultRules ? defaultRules : [])
checkout.scan(item1)
checkout.scan(item2)
checkout.scan(item3)
const names1 = checkout.items.map(item => item.name);
console.log('Customers: Default Items Scanned: '
  + names1 + ' Total expected: $' + checkout.total())

const secondBiteRules = rules.get('SecondBite')
checkout.clear();
checkout.changeRules(secondBiteRules ? secondBiteRules : []);

checkout.scan(item1)
checkout.scan(item1)
checkout.scan(item1)
checkout.scan(item3)
const names2 = checkout.items.map(item => item.name);
console.log('Customers: SecondBite Items Scanned: '
  + names2 + ' Total expected: $' + checkout.total())

const axilCoffeeRoastersRules = rules.get('Axil Coffee Roasters');
checkout.clear();
checkout.changeRules(axilCoffeeRoastersRules ? axilCoffeeRoastersRules : []);

checkout.scan(item2)
checkout.scan(item2)
checkout.scan(item2)
checkout.scan(item3)
const names3 = checkout.items.map(item => item.name);
console.log('Customers: Axil Coffee Roasters Items Scanned: '
  + names3 + ' Total expected: $' + checkout.total())