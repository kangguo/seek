import Item from './Item'
import PricingRule from './PricingRule'
import { Accumulator } from './Utils'

export default class Checkout {
    static new = (pricingRules: PricingRule[]) => {
        return new Checkout(pricingRules)
    }

    items: Item[] = []
    pricingRules: PricingRule[]

    constructor(pricingRules: PricingRule[]) {
        this.pricingRules = pricingRules
    }

    changeRules = (pricingRules: PricingRule[]) => {
        this.pricingRules = pricingRules
    }

    scan = (item: Item) => {
        this.items = [...this.items, item];
    }
    clear = () => {
        this.items = []
    }
    total = () => {
        const reducer = (accumulator: Accumulator, pricingRule: PricingRule) => {
            const items = accumulator.leftItems
                .filter(item => item.name === pricingRule.name)
            const itemCount = items.length;
            const originalPrice = items[0] ? items[0].price : 0.0

            return {
                total: accumulator.total + pricingRule.getItemTotal(itemCount, originalPrice),
                leftItems: accumulator.leftItems.filter(item => item.name !== pricingRule.name)
            }
        }

        //calculate skus with pricing rules
        const result = this.pricingRules
            .reduce(reducer, { total: 0.0, leftItems: this.items });

        //calculate skus without pricing rules
        return result.leftItems
            .reduce((total: number, item) => total + item.price, result.total);
    }
}