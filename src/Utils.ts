import Item from './Item'
export interface Accumulator {
    total: number
    leftItems: Item[]
}

export type ItemTotalFunction = (itemCount: number, orignalPrice: number) => number

export const calcN4M = (itemCount: number, price: number,
    packageSize: number, freeCountPerPackage: number) => {

    const packageCount = Math.floor(itemCount / packageSize)
    const normalCount = itemCount % packageSize
    const packagePrice = packageCount * (packageSize - freeCountPerPackage) * price

    return packagePrice + normalCount * price
}

export const calcWithDiscount = (itemCount: number, discountPrice: number) => {
    return itemCount * discountPrice
}