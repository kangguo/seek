export default class Item {
    name = ''
    description = ''
    price = 0.00

    constructor(name: string, description: string, price: number) {
        this.name = name
        this.description = description
        this.price = price
    }
}