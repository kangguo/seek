import { ItemTotalFunction } from './Utils'
export default class PricingRule {
    name: string = ''
    getItemTotal: ItemTotalFunction = () => 0.0

    constructor(name: string, getItemTotal: ItemTotalFunction) {
        this.name = name
        this.getItemTotal = getItemTotal
    }
}